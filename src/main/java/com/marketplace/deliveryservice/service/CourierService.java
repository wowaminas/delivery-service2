package com.marketplace.deliveryservice.service;


import com.marketplace.deliveryservice.entity.Courier;
import com.marketplace.deliveryservice.repository.CourierRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;


/**
 * Класс Курьер для поиска курьера по id
 */

@Service
@Slf4j
@AllArgsConstructor
public class CourierService {

    private final CourierRepository courierRepository;

    public Courier findCourierById(UUID id) {
        Optional<Courier> courierResponse = courierRepository.findById(id);
        Courier courier = courierResponse.get();
        return courier;
    }
}
