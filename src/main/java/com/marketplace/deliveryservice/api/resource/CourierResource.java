package com.marketplace.deliveryservice.api.resource;


import com.marketplace.deliveryservice.api.dto.CourierDto;
import com.marketplace.deliveryservice.entity.Courier;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.tags.Tags;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;


import javax.validation.constraints.NotNull;
import java.util.UUID;

@Tags(
        value = {
                @Tag(name = "Курьер", description = "Работа с курьером")
        }
)
@RequestMapping("/courier")
public interface CourierResource {

    @GetMapping("/{id}")
    @Operation(summary = "Вывод курьера по id", tags = "Курьер")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Курьер найден",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = Courier.class))}),
            @ApiResponse(responseCode = "400", description = "Неверный запрос",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Курьер не найден",
                    content = @Content)})
    ResponseEntity<CourierDto> findByCourierId(@PathVariable @NotNull UUID id);
}
