package com.marketplace.deliveryservice.api.exception;

import java.util.UUID;

public class CourierNotFoundException extends RuntimeException {

    public CourierNotFoundException(String massage, UUID id) {
        super(massage);
    }
}
