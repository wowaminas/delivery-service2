package com.marketplace.deliveryservice.api.util;


import com.marketplace.deliveryservice.api.dto.CourierDto;
import com.marketplace.deliveryservice.entity.Courier;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;


@Service
@AllArgsConstructor
public class CourierMapper {

    private final ModelMapper modelMapper;

    public CourierDto convertItemToDto (Courier courier) {

        return modelMapper.map(courier, CourierDto.class);
    }

    public Courier convertDtoToItem(CourierDto courierDto) {

        return modelMapper.map(courierDto, Courier.class);
    }
}
